<?php

namespace Scigeniq\Log\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Scigeniq\Notifier\Contracts\Notifiable;

class LogEvent implements Notifiable
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $level;
    public $message;
    public $context;
    public $color;

    protected  $status_colors = array(
        'info' => '#00B945',
        'warning' => '#ff9f00',
        'error' => '#BC001A'
    );

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($level, $message, $context)
    {
        $this->level = $level;
        $this->message = $message;
        $this->context= $context;
        $this->color =  isset($this->status_colors[$level]) ? $this->status_colors[$level] : '#000000';
    }

    public static function getAvailableFields():array
    {
        return ['level', 'message', 'context', 'color'];
    }

    /**
     * Return array of data for notifications where keys ara field's name
     *
     * @return array
     */
    public function getFieldsData(): array
    {
        return [
            'level' => $this->level,
            'message' => $this->message,
            'color' => $this->color,
            'context' => $this->context
        ];
    }

    /**
     * Return array with files
     *
     * @return array
     */
    public static function getAvailableFiles(): array
    {
        return [];
    }
}
