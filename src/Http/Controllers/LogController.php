<?php

namespace Scigeniq\Log\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Rap2hpoutre\LaravelLogViewer\LogViewerController;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

use Rap2hpoutre\LaravelLogViewer\LaravelLogViewer;
use Scigeniq\Dashboard\Dashboard;
use Scigeniq\Dashboard\Elements\Boxes\Box;
use Scigeniq\Dashboard\Elements\Breadcrumbs\Breadcrumbs;


class LogController extends LogViewerController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Show log page in dashboard
     *
     */
    public function index()
    {
        $laravelLogViewer = new LaravelLogViewer();

        if (Request::input('l')) {
            $laravelLogViewer->setFile(base64_decode(Request::input('l')));
        }

        if (Request::input('dl')) {
            return Response::download($laravelLogViewer->pathToLogFile(base64_decode(Request::input('dl'))));
        } elseif (Request::has('del')) {
            File::delete($laravelLogViewer->pathToLogFile(base64_decode(Request::input('del'))));
            return Redirect::to(Request::url());
        }

        $logs = $laravelLogViewer->all();
        $current_file  = $laravelLogViewer->getFileName();
        $files  = $laravelLogViewer->getFiles(true);

        $files = $this->orderFileLogsByName($files);

        $content = view('log::log', compact(
            'logs',
            'files',
            'current_file'
        ));

        /** @var Dashboard $dashboard */
        $dashboard = app()->make(Dashboard::class);

        $box = (new Box($content))->headerAvailable(false);

        $dashboard->content($box);

        return $dashboard->render();
    }

    /**
     * @param array $files
     * @return array
     */
    protected function orderFileLogsByName(array $files): array
    {
        sort($files, SORT_NATURAL);

        return $files;
    }

}
