<?php

namespace Scigeniq\Log;


use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Illuminate\Log\Events\MessageLogged;
use Illuminate\Support\Facades\Event;
use Scigeniq\Log\Events\LogEvent;

class LogEventServiceProvider extends EventServiceProvider
{
    /**
     * NotifierEventServiceProvider constructor.
     *
     * @internal param array $listen
     */
    public function __construct($app)
    {
        parent::__construct($app);

    }

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen(MessageLogged::class, function(MessageLogged $e) {
            $level = $e->level;
            $available_levels = config('scigeniq.dashboard.log.available_levels_log');
            if(in_array($level, $available_levels)){
                $message = $e->message;
                $context = count($e->context) ? json_encode($e->context['exception']->getTrace()) : '[]';
                LogEvent::dispatch($level, $message, $context);
            }
        });
    }
}
